package api

import (
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type StorageList *[]Question

type SourceJSON struct {
	filename string
	mu       sync.Mutex
}

func NewSourceJSON(location string) *SourceJSON {
	return &SourceJSON{
		filename: location,
	}
}

func (s SourceJSON) LoadQuestions() (*QuestionList, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	b, err := ioutil.ReadFile(s.filename)
	if err != nil {
		return nil, err
	}
	var lst StorageList
	err = json.Unmarshal(b, &lst)
	if err != nil {
		return nil, err
	}

	ql := &QuestionList{
		Data: lst,
	}
	return ql, nil
}

func (s SourceJSON) StoreQuestions(qs *[]Question) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	f, _ := json.Marshal(qs)
	err := ioutil.WriteFile(s.filename, f, 0644)
	return err
}

type SourceCSV struct {
	filename string
	mu       sync.Mutex
}

func NewSourceCSV(location string) *SourceCSV {
	return &SourceCSV{
		filename: location,
	}
}

func (s SourceCSV) LoadQuestions() (*QuestionList, error) {
	s.mu.Lock()
	f, err := os.Open(s.filename)
	if err != nil {
		s.mu.Unlock()
		return nil, err
	}
	defer f.Close()

	reader := csv.NewReader(f)
	reader.TrimLeadingSpace = true // allow a space after the separator
	records, err := reader.ReadAll()
	if err != nil {
		s.mu.Unlock()
		return nil, err
	}
	s.mu.Unlock()

	var lst []Question
	for _, r := range records[1:] { // skip first record (CSV header)
		var choices []Choice
		for _, c := range r[2:5] {
			txt := c
			choices = append(choices, Choice{Text: &txt})
		}
		ts, err := time.Parse("2006-01-02 15:04:05", r[1])
		if err != nil {
			return nil, err
		}
		q := Question{
			Text:      &r[0],
			Choices:   &choices,
			CreatedAt: &ts,
		}
		lst = append(lst, q)
	}
	ql := &QuestionList{
		Data: &lst,
	}
	return ql, nil
}

func (s SourceCSV) StoreQuestions(qs *[]Question) error {
	data := prepareCSV(qs)
	s.mu.Lock()
	f, err := os.OpenFile(s.filename, os.O_WRONLY, 0644)
	if err != nil {
		s.mu.Unlock()
		return err
	}
	defer f.Close()

	writer := csv.NewWriter(f)
	for _, row := range *data {
		_ = writer.Write(row)
	}
	writer.Flush()
	s.mu.Unlock()

	return nil
}

func prepareCSV(qs *[]Question) *[][]string {
	res := make([][]string, 1)
	res[0] = []string{"Question text", "Created At", "Choice 1", "Choice 2", "Choice 3"}
	for _, q := range *qs {
		ts := *q.CreatedAt
		record := []string{*q.Text, ts.Format("2006-01-02 15:04:05")}
		for _, choice := range *q.Choices {
			record = append(record, *choice.Text)
		}
		res = append(res, record)
	}
	return &res
}
