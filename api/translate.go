package api

import (
	"context"
	"fmt"

	"cloud.google.com/go/translate"
	"golang.org/x/text/language"
)

func translateQs(targetLang language.Tag, qs *[]Question) (*QuestionList, error) {
	ctx := context.Background()

	client, err := translate.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	var translatedList []Question
	for _, q := range *qs {
		toTranslate := []string{*q.Text}
		for _, c := range *q.Choices {
			toTranslate = append(toTranslate, *c.Text)
		}
		resp, err := client.Translate(ctx, toTranslate, targetLang, nil)
		if err != nil {
			return nil, fmt.Errorf("Translate: %v", err)
		}
		var translatedChoices []Choice
		for _, tc := range resp[1:] {
			choice := tc
			translatedChoices = append(translatedChoices, Choice{Text: &choice.Text})
		}

		translatedQ := Question{
			Text:      &resp[0].Text,
			Choices:   &translatedChoices,
			CreatedAt: q.CreatedAt,
		}

		translatedList = append(translatedList, translatedQ)
	}

	return &QuestionList{Data: &translatedList}, nil
}
