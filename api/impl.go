package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"golang.org/x/text/language"
)

type Storage interface {
	LoadQuestions() (*QuestionList, error)
	StoreQuestions(*[]Question) error
}

type Questionnaire struct {
	Storage Storage
}

func NewQuestionnaire(source string) *Questionnaire {
	var s Storage
	if source == "json" {
		s = NewSourceJSON("questions.json")
	} else if source == "csv" {
		s = NewSourceCSV("questions.csv")
	}
	q := &Questionnaire{
		Storage: s,
	}

	return q
}

func (q *Questionnaire) GetQuestions(c echo.Context, params GetQuestionsParams) error {
	qs, err := q.Storage.LoadQuestions()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("Error: %s", err))
	}
	lang, err := language.Parse(params.Lang)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid language parameter: %s", params.Lang))
	}
	if lang.String() != "en" {
		qs, err = translateQs(lang, qs.Data)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("Failed to translate: %s", err))

		}
	}
	return c.JSON(http.StatusOK, qs)
}

func (q *Questionnaire) PostQuestions(c echo.Context) error {
	newQuestion := new(Question)
	if err := c.Bind(newQuestion); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("Error: %s", err))
	}
	now := time.Now()
	newQuestion.CreatedAt = &now

	qs, err := q.Storage.LoadQuestions()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("Error: %s", err))
	}
	newList := *qs.Data
	newList = append(newList, *newQuestion)
	err = q.Storage.StoreQuestions(&newList)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("Error: %s", err))
	}

	return c.JSON(http.StatusOK, newQuestion)
}
