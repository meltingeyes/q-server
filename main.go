package main

import (
	"os"

	"bitbucket.org/meltingeyes/q-server/api"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// setting up a router
	e := echo.New()
	e.Use(middleware.Logger())

	source := os.Getenv("SOURCE")
	if source == "" {
		source = "json"
	}

	qList := api.NewQuestionnaire(source)
	api.RegisterHandlers(e, qList)
	e.Logger.Fatal(e.Start(":1323"))
}
