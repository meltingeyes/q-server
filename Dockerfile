FROM golang:1.14

WORKDIR /go/src/app
COPY . .

RUN make build

CMD ["./main"]