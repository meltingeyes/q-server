build:
	go build main.go
run:
	./main
generate-server:
	oapi-codegen -package api -generate server,types api/open-api.yaml >api/server.gen.go
