## Questionnaire server

This is a simple API that provides an endpoint for 2 types of requests:
adding a new multiple-choice question and listing every question in a database.

The API provides translation feature that uses Google Cloud Translation API.
In order to enable this feature you should have GCP Service Account
credentials file that needs to be exposed via environment variable.

There are two file storage back-ends available: JSON (default) and CSV. You
can choose between the two at time start up via `SOURCE` environment variable.

### Running

```$ docker build -t q-server .```

```$ docker run --rm -e GOOGLE_APPLICATION_CREDENTIALS=creds.json -p 1323:1323 -it q-server```

or with CSV back-end

```$ docker run --rm -e GOOGLE_APPLICATION_CREDENTIALS=creds.json -e SOURCE=csv -p 1323:1323 -it q-server```

Now you may try to call it in another bash session:

```$ curl -s localhost:1323/questions?lang=en```

try with translation

```$ curl -s localhost:1323/questions?lang=ru```

post a new question

```$ curl -s -H 'Content-Type: application/json'
    -d '{"choices":[{"text":"4"},{"text":"5"},{"text":"42"}],"text":"2 + 2 = ?"}'
    localhost:1323/questions
```
