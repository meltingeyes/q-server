module bitbucket.org/meltingeyes/q-server

go 1.14

require (
	cloud.google.com/go v0.55.0
	github.com/deepmap/oapi-codegen v1.3.6
	github.com/labstack/echo/v4 v4.1.11
	golang.org/x/text v0.3.2
)
